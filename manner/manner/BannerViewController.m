//
//  BannerViewController.m
//  manner
//
//  Created by takumi ninomiya on 2012/11/01.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import "BannerViewController.h"

#define MY_BANNER_UNIT_ID @"a14fb5a67041061";

@implementation BannerViewController


-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    
    CGRect bannerFrame = [UIScreen mainScreen].applicationFrame;
    UIView *view = [[UIView alloc] initWithFrame:bannerFrame];
    view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.view = view;
    
    [view release];
    
    bannerView_.frame = CGRectMake(0 , view.frame.size.height - 48, view.frame.size.width, 48);
    
    bannerView_.adUnitID = MY_BANNER_UNIT_ID;
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];
    
    GADRequest *_request = [GADRequest request];
    _request.testing = YES;
    [bannerView_ loadRequest:_request];
    
}

-(void)dealloc{
    [bannerView_ release];
    [super dealloc];
}

@end
