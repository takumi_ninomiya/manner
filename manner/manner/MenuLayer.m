//
//  MenuLayer.m
//  manner
//
//  Created by takumi ninomiya on 2012/11/07.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import "MenuLayer.h"

@implementation MenuLayer

+ (CCScene *) scene{
    CCScene *scene = [CCScene node];
    MenuLayer *layer = [MenuLayer node];
    
    [scene addChild:layer];
        
    return scene;
}

-(id)init{
    self = [super init];
    
    if(self){
        
        [self setTouchEnabled:YES];
    
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        //画像の表示
        //スプライトの作成
        CCSprite *backgroundImage = [CCSprite spriteWithFile:@"Default.png"];
        
        //画像の配置場所を指定
        backgroundImage.position = CGPointMake(winSize.width / 2, winSize.height / 2);
        [self addChild:backgroundImage];
        /*
        CCSprite *gameBtn = [CCSprite spriteWithFile:@"person01.png"];
        gameBtn.position = CGPointMake(winSize.width / 2, winSize.height / 2);
        gameBtn.scale = 0.6f;
        [self addChild:gameBtn];
        
        CCSprite *btn_back = [CCSprite spriteWithFile:@"bectle.png"];
        btn_back.scale = 0.4f;
        btn_back.flipX = YES;
        btn_back.visible = NO;
        btn_back.position = CGPointMake(0 + (btn_back.contentSize.width * btn_back.scale) / 2, winSize.height / 2);
        [self addChild:btn_back];
        
        CCSprite *btn_next = [CCSprite spriteWithFile:@"bectle.png"];
        btn_next.scale = 0.4f;
        btn_next.position = CGPointMake(winSize.width - (btn_back.contentSize.width * btn_back.scale) / 2, winSize.height / 2);
        [self addChild:btn_next];
         */
        
        CCMenuItemImage *btn_back = [CCMenuItemImage itemWithNormalImage:@"bectle.png"
                                                           selectedImage:@"bectle.png"
                                                                  target:nil
                                                                selector:@selector(scrollPrev:)
                                     ];
        btn_back.scale = 0.4f;
        btn_back.position =  ccp((-1 * winSize.width / 2) + (btn_back.contentSize.width * btn_back.scale) - 50 , 0);
        btn_back.rotation = 180;
        
        CCMenuItemImage *btn_next = [CCMenuItemImage itemWithNormalImage:@"bectle.png"
                                                           selectedImage:@"bectle.png"
                                                                  target:nil
                                                                selector:@selector(scrollPrev:)
                                     ];
        btn_next.scale = 0.4f;
        btn_next.position =  ccp((winSize.width / 2) + (btn_back.contentSize.width * btn_back.scale)  - 50 , 0);
        
        
        CCMenu *menu = [CCMenu menuWithItems:btn_back , btn_next , nil];
        [self addChild:menu];
        
    }
    
    return self;
}


-(void)scrollPrev:(id)sender{
    CCLOG(@"touch");
}


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //CCLOG(@"touch began is %@" , event);
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    //CCLOG(@"touch end is %@" , event);
}


-(void) dealloc {
    
    [super dealloc];
}

@end
