//
//  TopLayer.h
//  manner
//
//  Created by takumi ninomiya on 2012/10/30.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "MenuLayer.h"

@interface TopLayer : CCLayer{
    
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
