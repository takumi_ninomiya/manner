//
//  TopLayer.m
//  manner
//
//  Created by takumi ninomiya on 2012/10/30.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import "TopLayer.h"
#import "AppDelegate.h"

@implementation TopLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	TopLayer *layer = [TopLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

//
-(id) init
{
	if( (self=[super init])) {
        
        [self setTouchEnabled:YES];
        
        //self.isTouchEnabled = YES;
        
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        
        
        
		CCSprite *background;
		
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
			background = [CCSprite spriteWithFile:@"Default.png"];
		} else {
			background = [CCSprite spriteWithFile:@"Default-ipadLand@2x.png"];
		}
		background.position = ccp(size.width/2, size.height/2);
        
		// add the label as a child to this Layer/Users/takumi_ninomiya/worksData/manner/manner/TopLayer.m
		[self addChild: background];
        
        
        CCMenuItemImage *start = [CCMenuItemImage
                                  itemFromNormalImage:@"top_image.png"
                                  selectedImage:@"top_image.png"
                                  target:self
                                  selector:@selector(pressMenuItem:)
                                  ];
        
        CCMenu *menu = [CCMenu menuWithItems:start, nil];
        [self addChild: menu];
	}
	return self;
}

-(void)pressMenuItem:(id)sender{
    //NSLog(@"%s",__func__);
    [self makeTransition:0];
}


-(void) registerWithTouchDespatcher{
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self
                                                     priority:0
                                              swallowsTouches:YES];
}

-(BOOL)cctouchBegan:(UITouch *)touch widthEvent:(UIEvent *)event{
    return YES;
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event{
    
}

-(void)makeTransition:(ccTime)dt
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[MenuLayer scene] withColor:ccWHITE]];
}


-(void) dealloc {
    
    [super dealloc];
}


@end
